from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='helloworld',
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    test_suite='tests',
    entry_points={
        'console_scripts':
            ['Lab2 = main.core:main']
        },
)
