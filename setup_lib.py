from setuptools import setup, find_packages
from os.path import join, dirname

root = 'main.lib'
packages = [root] + [(root + '.' + subfolder) for subfolder in find_packages(root)]

setup(
    name='tasktracker_lib',
    version='1.0',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    packages=packages,
    include_package_data=False,
    url='https://bitbucket.org/xsedden/tracker/src/master/main/lib/',
)
