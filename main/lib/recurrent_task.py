# -*- coding: utf-8 -*-

import calendar
import logging
import subprocess
import os

from datetime import datetime, date, timedelta

from main.lib import exceptions
from main.lib.config import take_text_path
from main.lib.task import Task
from main.lib.person import check_login, get_login_person
from main.lib.convert_data import add_task_to_file, get_rec_text_by_id, remove_rec_task_by_id, set_recc_task, \
    take_last_id_rec, get_rec_ids, get_rec_type_by_id, get_rec_name_by_id, get_rec_nextdate_by_id,\
    get_rec_famaly_by_id, get_rec_priority_by_id, set_rec_nextdate_by_id, get_rec_creater_by_id, check_rec_id


class Recurrent:

    def __init__(self, typetask, text, famaly, name, startdate):
        self.rec_name = name
        self.rec_text = text
        self.rec_famaly = famaly
        self.rec_startdate = startdate
        self.rec_typetask = typetask
        self.rec_priority = 10

        self.set_create_date()
        self.set_text(text)
        self.set_id()


    def set_create_date(self):
        """  Set create date """
        self.create_date = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")


    def set_text(self,text):
        """  Set path for text  """
        if text:
            subprocess.call(['touch', os.path.join(take_text_path(), text)])
            subprocess.call(['vim', os.path.join(take_text_path(), text)])
            self.task_text = os.path.join(take_text_path(), text)


    def set_id(self):
        """  Set id (inc last id) """
        self.rec_id =  take_last_id_rec() + 1


    def add_rec(self):
        """  Add recurrent task in data base """
        check_login()
        set_recc_task(self.rec_id, self.rec_typetask, self.rec_text, self.rec_famaly, self.create_date, self.rec_name, self.rec_startdate, self.rec_priority)
        logging.info("recurrent task {0} was created by {1}".format(self.rec_id, get_login_person()))


def remove_rec(rec_id):
    """  Remove recurrent task """
    if not check_rec_id(rec_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    remove_rec_task_by_id(rec_id)
    logging.info("recurrent task {0} was removed by {1}".format(rec_id, get_login_person()))


def check_rec():
    """  Check all recurrent task for {next date} if was {next date} create new task and set new {next date}"""
    now = datetime.now()
    for rec_id in get_rec_ids():
        if get_rec_type_by_id(rec_id) == "d":
            now2 = int(datetime.strftime(now + timedelta(days=1), "%Y%m%d"))
        if get_rec_type_by_id(rec_id) == "w":
            now2 = int(datetime.strftime(now + timedelta(weeks=1), "%Y%m%d"))
        if get_rec_type_by_id(rec_id) == "m":
            now2 = int(datetime.strftime(add_months(now, 1), "%Y%m%d"))
        while now2 > get_rec_nextdate_by_id(rec_id):
            add_task(rec_id)
            d = date(int(int(get_rec_nextdate_by_id(rec_id)) / 10000),
                     int((int(get_rec_nextdate_by_id(rec_id)) / 100) % 100),
                     int(int(get_rec_nextdate_by_id(rec_id)) % 100))
            if get_rec_type_by_id(rec_id) == "d":
                set_rec_nextdate_by_id(rec_id, int(datetime.strftime(d + timedelta(days=1), "%Y%m%d")))
            if get_rec_type_by_id(rec_id) == "w":
                set_rec_nextdate_by_id(rec_id, int(datetime.strftime(d + timedelta(weeks=1), "%Y%m%d")))
            if get_rec_type_by_id(rec_id) == "m":
                set_rec_nextdate_by_id(rec_id, int(datetime.strftime(add_months(d, 1), "%Y%m%d")))


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + int(month / 12))
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return date(year, month, day)


def add_task(rec_id):
    task = Task(get_rec_name_by_id(rec_id), get_rec_text_by_id(rec_id), get_rec_famaly_by_id(rec_id),
        get_rec_priority_by_id(rec_id), get_rec_nextdate_by_id(rec_id), get_rec_creater_by_id(rec_id))
    add_task_to_file(task)
