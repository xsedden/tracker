# -*- coding: utf-8 -*-
import logging
import subprocess
from datetime import datetime

from main.lib.person import check_login, check_access_admin, check_access_creater ,get_login_person
from main.lib import exceptions
from main.lib.convert_data import get_tasks_ids, get_deadline_by_id, delete_task_by_id, \
    get_status_by_id,check_id, \
    set_stasus_by_id, get_ids_by_famaly, get_text_by_id, set_deadline_by_id, set_famaly_by_id, get_ids_parents_by_id, \
    set_name_by_id, set_priority_by_id


def finish_task(task_id):
    """ Recurrent finish status task by id. if task was finished edit status to excepts. 1st. arg id task """
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        status = get_status_by_id(task_id)
        if status != 1:
            status = 1
        else:
            status = None
        set_stasus_by_id(task_id, status)
        _rec_finish_task(task_id)
        logging.info("task {0} was finished by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def _rec_finish_task(famaly):
    ids = get_ids_by_famaly(famaly)
    for task_id in ids:
        status = get_status_by_id(task_id)
        if status != 1:
            status = 1
        else:
            status = None
        set_stasus_by_id(task_id, status)
        _rec_finish_task(task_id)


def delete_task(task_id):
    """ Recurrent delete task by id. 1st. arg id task """
    if isinstance(task_id, list):
        for task in task_id:
            if not check_id(task):
                raise exceptions.IDError('Не коректный ID.')
            if check_access_creater(task):
                son = check_son(task)
                if son:
                    delete_task(son)
                delete_task_by_id(task)
                logging.info("task {0} was removed by {1}".format(task, get_login_person()))
            else:
                raise exceptions.UserError('Недостаточно прав пользователя.')
    else:
        if not check_id(task_id) and not task_id is None:
            raise exceptions.IDError('Не коректный ID.')
        if check_access_creater(task_id):
            son = check_son(task_id)
            if son:
                delete_task(son)
            delete_task_by_id(task_id)
            logging.info("task {0} was removed by {1}".format(task_id, get_login_person()))
        else:
            raise exceptions.UserError('Недостаточно прав пользователя.')


def check_son(task_id):
    return get_ids_by_famaly(task_id)


def edit_text(task_id):
    """ Edit  text for task by id. 1st. arg id task """
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        if get_text_by_id(task_id):
            subprocess.call(['vim', get_text_by_id(task_id)])
            logging.info("text from task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def convert_date(date):
    return date.replace(".", "")[:8]


def check_deadline():
    """ Check deadlines for all tasks. Edit status if was deadline. """
    now = datetime.strftime(datetime.now(), "%Y%m%d")
    for task_id in get_tasks_ids():
        if get_deadline_by_id(task_id) < int(now) and get_status_by_id(task_id) is None:
            set_stasus_by_id(task_id, 2)
            logging.info("was deadline for task {}".format(task_id))


def edit_deadline(task_id, deadline):
    """ Edit deadline for task by id. 1st. arg id task, 2nd new deadline"""
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        set_deadline_by_id(task_id, deadline)
        logging.info("deadline for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def edit_name(task_id, name):
    """ Edit name for task by id. 1st. arg id task, 2nd new name"""
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        set_name_by_id(task_id, name)
        logging.info("name for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')


def edit_priority(task_id, priority):
    """ Edit priority for task by id. 1st. arg id task, 2nd new priority"""
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        set_priority_by_id(task_id, priority)
        logging.info("priority for task {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')



def edit_famaly(task_id, famaly):
    """ Edit parent for task by id. 1st. arg id task, 2nd new id parent"""
    if not check_id(task_id):
        raise exceptions.IDError('Не коректный ID.')
    check_login()
    if check_access_admin(task_id):
        if task_id in get_ids_parents_by_id(famaly):
            raise exceptions.TaskError('Узбагойся. Так делать не н-н-н-ннада=)')
        else:
            set_famaly_by_id(task_id, famaly)
            logging.info("famaly for {0} was edit by {1}".format(task_id, get_login_person()))
    else:
        raise exceptions.UserError('Недостаточно прав пользователя.')

