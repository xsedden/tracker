import subprocess

from main.lib.convert_data import get_text_by_id, get_ids_parents_by_id, get_tasks_id_by_sort, get_famaly_by_id, \
    get_status_by_id, get_name_by_id, get_deadline_by_id, get_createdate_by_id, get_priority_by_id
from main.lib.edit import check_deadline, convert_date
from main.lib.person import check_show, check_login, check_access_person
from main.lib.recurrent_task import check_rec


def show_all(args):
    """  Show all tasks as tree """
    check_login()
    check_rec()
    check_deadline()
    out_list = []
    for task_id in get_tasks_id_by_sort(args):
        if get_famaly_by_id(task_id) is None:
            if check_show(task_id):
                if not args.showbyinterval:
                    out_list.append(task_id)
                elif args.showbyinterval[0] <= convert_date(get_createdate_by_id(task_id)) and args.showbyinterval[1] >= convert_date(get_createdate_by_id(task_id)):
                    out_list.append(task_id)
            out_list = _rec_show(task_id, get_tasks_id_by_sort(args),args, out_list)
    return out_list


def _rec_show(task_id, ids, args, out_list):
    for task in ids:
        if get_famaly_by_id(task) == task_id:
            if check_show(task):
                if not args.showbyinterval:
                    out_list.append(task_id)
                elif args.showbyinterval[0] <= convert_date(get_createdate_by_id(task_id)) and args.showbyinterval[1] >= convert_date(get_createdate_by_id(task_id)):
                    out_list.append(task_id)
            out_list =_rec_show(task, ids, args,out_list)
    return out_list


def show_text(task_id):
    """  Show text by id task """
    if check_access_person(task_id):
        subprocess.call(['cat', get_text_by_id(task_id)])
    else:
        check_login()


def show_active(args):
    """  Show active tasks as tree """
    check_login()
    check_rec()
    check_deadline()
    out_list = []
    for task_id in get_tasks_id_by_sort(args):
        # exepts
        if get_famaly_by_id(task_id) is None and get_status_by_id(task_id) != 1:
            if check_show(task_id):
                if not args.showbyinterval:
                    out_list.append(task_id)
                elif args.showbyinterval[0] <= convert_date(get_createdate_by_id(task_id)) and args.showbyinterval[1] >= convert_date(get_createdate_by_id(task_id)):
                    out_list.append(task_id)
            out_list = _rec_active_show(task_id, get_tasks_id_by_sort(args),args, out_list)
    return out_list


def _rec_active_show(task_id, ids,args, out_list):
    for task in ids:
        if get_famaly_by_id(task) == task_id and get_status_by_id(task) != 1:
            if check_show(task):
                if not args.showbyinterval:
                    out_list.append(task_id)
                elif args.showbyinterval[0] <= convert_date(get_createdate_by_id(task_id)) and args.showbyinterval[1] >= convert_date(get_createdate_by_id(task_id)):
                    out_list.append(task_id)
            out_list = _rec_active_show(task, ids, args, out_list)
    return out_list


def convert_date(date):
    return date.replace(".", "")[:8]
