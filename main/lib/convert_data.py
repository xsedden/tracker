# -*- coding: utf-8 -*-
import json
import os
import logging
import subprocess
from datetime import datetime

from main.lib.config import take_login_path, take_users_path, take_recurrent_path
from main.lib.config import take_tasks_path, take_text_path
from main.lib.sort import sort_by_priority, sort_by_deadline, sort_by_create_date
from main.lib import exceptions


def add_task_to_file(task):
    jsondata = from_file()
    jsondata.append({
        'task_id': task.task_id,
        'name': task.task_name,
        'text': task.task_text,
        'famaly': task.task_famaly,
        'status': task.task_status,
        'priority': task.task_priority,
        'createdate': task.create_date,
        'deadline': task.task_deadline,
        "persons": {
            'creater': task.task_creater,
            'admin': [task.task_creater],
            'person': [task.task_creater]
            }
        })
    save_to_task_file(jsondata)
    logging.info("task {0} was created by {1}".format(task.task_id, task.task_creater))


def take_last_id():
    data = from_file()
    if data:
        return data.pop().get("task_id")
    else:
        return 0


def from_file():
    return from_file_by_path(take_tasks_path())


def from_file_by_path(path):
    # print ("Json")
    file = open(path, 'r')
    jsondata = file.read()
    file.close()
    jsondata = json.loads(jsondata)
    # print (jsondata["name"])
    return jsondata


def save_to_task_file(jsondata):
    save_by_path(jsondata, take_tasks_path())


def save_by_path(jsondata, path):
    jsondata = json.dumps(jsondata, sort_keys=True, indent=4)
    file = open(path, 'w')
    file.write(jsondata)
    file.close()


def get_tasks_id_by_sort(args):
    jsondata = from_file()
    if args.showbypriority:
        jsondata.sort(key=sort_by_priority)
    if args.showbydeadline:
        jsondata.sort(key=sort_by_deadline)
    if args.showbycreatedate:
        jsondata.sort(key=sort_by_create_date)
    out_list = list()
    if jsondata:
        for dic in jsondata:
            out_list.append(dic.get("task_id"))
    return out_list


def get_tasks_ids():
    jsondata = from_file()
    out_list = list()
    if jsondata:
        for dic in jsondata:
            out_list.append(dic.get("task_id"))
    return out_list


def get_tasks_by_famaly(task_id):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("famaly") == task_id:
            out_list.append(dic)
    return out_list


def get_ids_by_famaly(task_id):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("famaly") == task_id:
            out_list.append(dic.get("task_id"))
    return out_list


def get_active_tasks_by_famaly(task_id):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("famaly") == task_id and dic.get("status") != 1:
            out_list.append(dic)
    return out_list


def get_active_ids_by_famaly(task_id):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("famaly") == task_id and dic.get("status") != 1:
            out_list.append(dic.get("task_id"))
    return out_list


def get_task_by_name(name):
    jsondata = from_file()
    for dic in jsondata:
        if dic["name"] == name:
            return dic


"""
def get_something_by_id(func):
    def other_func():
        jsondata = FromFile()
        for dic in jsondata:
            if dic.get("task_id") == task_id:
                val = func(dic)
        return val
    return other_func
"""


def get_task_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic


def get_text_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("text")


def get_famaly_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("famaly")


def get_name_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("name")


def get_status_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("status")


def get_priority_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("priority")


def get_createdate_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("createdate")


def get_deadline_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("deadline")


def get_person_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic["persons"]["person"]


def get_admin_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("persons").get("admin")


def get_creater_by_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return dic.get("persons").get("creater")


def save_task_by_id(task_id, task):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            out_list.append(task)
    save_to_task_file(out_list)


def set_name_by_id(task_id, name):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'name': name})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_famaly_by_id(task_id, famaly):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'famaly': famaly})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_stasus_by_id(task_id, status):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'status': status})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_priority_by_id(task_id, priority):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'priority': priority})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_createdate_by_id(task_id, createdate):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'createdate': createdate})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_deadline_by_id(task_id, deadline):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic.update({'deadline': deadline})
            out_list.append(dic)
    save_to_task_file(out_list)


def set_person_by_id(task_id, person):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic["persons"]["person"].append(person)
            out_list.append(dic)
    save_to_task_file(out_list)


def set_admin_by_id(task_id, admin):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            dic["persons"]["admin"].append(admin)
            dic["persons"]["person"].append(admin)
            out_list.append(dic)
    save_to_task_file(out_list)


def remove_person_by_id(task_id, person):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            if person in dic["persons"]["person"]:
                dic["persons"]["person"].remove(person)
            if person in dic["persons"]["admin"]:
                dic["persons"]["admin"].remove(person)
            out_list.append(dic)
    save_to_task_file(out_list)


def remove_admin_by_id(task_id, admin):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
        else:
            if admin in dic["persons"]["admin"]:
                dic["persons"]["admin"].remove(admin)
            out_list.append(dic)
    save_to_task_file(out_list)


def delete_task_by_id(task_id):
    jsondata = from_file()
    out_list = list()
    for dic in jsondata:
        if dic.get("task_id") != task_id:
            out_list.append(dic)
    save_to_task_file(out_list)


def set_login_password(login, password):
    jsondata = from_file_by_path(take_users_path())
    jsondata.append({'login': login, 'password': password})
    save_by_path(jsondata, take_users_path())


def check_log_pass(login, password):
    jsondata = from_file_by_path(take_users_path())
    for dic in jsondata:
        if dic["login"] == login and dic["password"] == password:
            return True
    raise exceptions.UserError('Неверные имя пользователя или пароль.')



def set_login_person(login):
    jsondata = from_file_by_path(take_login_path())
    jsondata["logperson"] = login
    save_by_path(jsondata, take_login_path())


def get_login_person():
    jsondata = from_file_by_path(take_login_path())
    if jsondata:
        return jsondata["logperson"]
    else:
        return "default"


def get_persons():
    jsondata = from_file_by_path(take_users_path())
    out_list = list()
    for dic in jsondata:
        out_list.append(dic["login"])
    return out_list


def exit_person_f():
    jsondata = from_file_by_path(take_login_path())
    jsondata["logperson"] = None
    save_by_path(jsondata, take_login_path())


def set_recc_task(rec_id, typetask, text, famaly, createdate, name, nextdate, priority):
    jsondata = from_file_by_path(take_recurrent_path())
    jsondata.append({
        'rec_id': rec_id,
        'typetask': typetask,
        'text': text,
        'famaly': famaly,
        'createdate': createdate,
        'name': name,
        'nextdate': nextdate,
        "priority": priority,
        "creater": get_login_person()
    })
    save_by_path(jsondata, take_recurrent_path())
    logging.info("Recurrent task {0} was created by {1} ".format(rec_id, get_login_person()))


def take_last_id_rec():
    data = from_file_by_path(take_recurrent_path())
    if data:
        return data.pop().get("rec_id")
    else:
        return 0


def get_rec_task_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic["rec_id"] == rec_id:
            return dic


def remove_rec_task_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    copydata = []
    for dic in jsondata:
        if dic["rec_id"] != rec_id:
            copydata.append(dic)
    save_by_path(copydata, take_recurrent_path())


def get_rec_creater_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic["rec_id"] == rec_id:
            return dic.get("creater")


def get_rec_text_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic["rec_id"] == rec_id:
            return dic.get("text")


def get_rec_ids():
    jsondata = from_file_by_path(take_recurrent_path())
    out_list = list()
    for dic in jsondata:
        out_list.append(dic.get("rec_id"))
    return out_list


def get_rec_type_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic.get("rec_id") == rec_id:
            return dic.get("typetask")


def get_rec_name_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic.get("rec_id") == rec_id:
            return dic.get("name")


def get_rec_nextdate_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic.get("rec_id") == rec_id:
            return dic.get("nextdate")


def get_rec_famaly_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic["rec_id"] == rec_id:
            return dic.get("famaly")


def get_rec_priority_by_id(rec_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic["rec_id"] == rec_id:
            return dic.get("priority")


def set_rec_nextdate_by_id(rec_id, nextdate):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic.get("rec_id") == rec_id:
            dic.update({"nextdate": nextdate})
    save_by_path(jsondata, take_recurrent_path())


def get_ids_parents_by_id(task_id):
    jsondata = from_file()
    out_list = list()
    buff_dic = dict()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            buff_dic = dic
    while not (buff_dic.get("famaly") is None):
        for d in jsondata:
            if d.get("task_id") == buff_dic.get("famaly"):
                out_list.append(d.get("task_id"))
                buff_dic = d
    return out_list


def check_id(task_id):
    jsondata = from_file()
    for dic in jsondata:
        if dic.get("task_id") == task_id:
            return True
    return False


def check_rec_id(task_id):
    jsondata = from_file_by_path(take_recurrent_path())
    for dic in jsondata:
        if dic.get("rec_id") == task_id:
            return True
    return False
