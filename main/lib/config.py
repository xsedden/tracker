from main.lib.logger import init_logger
import json
import os


def load_config(path_tmp = None, silence = False):
    if path_tmp:
        with open(os.path.join(os.path.expanduser('~/.track'), 'tmp.json'), 'w') as j_conf:
            json.dump({'path': path_tmp}, j_conf)
    config = __load_config_file()
    logs_path = take_logs_path()
    init_logger(logs_path, silence)
    return config


def __load_config_file():
    default_config_path = os.path.expanduser('~/.track/config/config.json')
    default_tmp_path = os.path.expanduser('~/.track/tmp.json')
    if os.path.exists(default_tmp_path):
        with open(default_tmp_path, 'r') as tmp:
            dic = tmp.read()
            dic = json.loads(dic)
            if os.path.exists(os.path.join(dic["path"], 'config/config.json')):
                with open(os.path.join(dic["path"], 'config/config.json'), 'r') as conf:
                    config = conf.read()
                    config = json.loads(config)
                    return config
            else:
                return __create_default_config()
    if os.path.exists(default_config_path):
        with open(default_config_path, 'r') as conf:
            config = conf.read()
            config = json.loads(config)
            return config
    else:
        return __create_default_config()


def take_dir_path():
    return __load_config_file()['workspace_path']


def take_users_path():
    return os.path.join(__load_config_file()['users_path'], 'users.json')


def take_tasks_path():
    return os.path.join(__load_config_file()['tasks_path'], 'tasks.json')


def take_logs_path():
    return __load_config_file()['logs_path']


def take_recurrent_path():
    return os.path.join(__load_config_file()['recurrent_path'], 'recurrent.json')


def take_login_path():
    return os.path.join(__load_config_file()['login_path'], 'login.json')


def take_text_path():
    return __load_config_file()['text_path']


def __create_default_config():
    default_tmp_path = os.path.expanduser('~/.track/tmp.json')
    default_dir = os.path.expanduser('~/.track')
    if os.path.exists(default_tmp_path):
        with open(default_tmp_path, 'r') as tmp:
            dic = tmp.read()
            dic = json.loads(dic)
            default_dir = dic['path']
    default_user_dir = os.path.join(default_dir, 'users')
    default_login_path = os.path.join(default_dir, 'login')
    default_cfg_path = os.path.join(default_dir, 'config')
    default_tasks_path = os.path.join(default_dir, 'tasks')
    default_logs_path = os.path.join(default_dir, 'logs')
    default_recurrent_path = os.path.join(default_dir, 'recurrent')
    default_text_dir = os.path.join(default_dir, 'text')

    def create_json_config():
        config = {
            'workspace_path': default_dir,
            'users_path': default_user_dir,
            'tasks_path': default_tasks_path,
            'logs_path': default_logs_path,
            'recurrent_path': default_recurrent_path,
            'login_path': default_login_path,
            'text_path': default_text_dir
        }
        with open(os.path.join(default_cfg_path, 'config.json'), 'w') as j_conf:
            json.dump(config, j_conf, indent=4, sort_keys=True)

    if not os.path.exists(default_dir):
        os.mkdir(default_dir)
    if not os.path.exists(default_text_dir):
        os.mkdir(default_text_dir)
    if not os.path.exists(default_user_dir):
        os.mkdir(default_user_dir)
        content = list()
        with open(os.path.join(default_user_dir, 'users.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_recurrent_path):
        os.mkdir(default_recurrent_path)
        content = list()
        with open(os.path.join(default_recurrent_path, 'recurrent.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_tasks_path):
        os.mkdir(default_tasks_path)
        content = list()
        with open(os.path.join(default_tasks_path, 'tasks.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_login_path):
        os.mkdir(default_login_path)
        content = {}
        with open(os.path.join(default_login_path, 'login.json'), 'w') as j_file:
            json.dump(content, j_file)
    if not os.path.exists(default_logs_path):
        os.mkdir(default_logs_path)
    if not os.path.exists(default_cfg_path):
        os.mkdir(default_cfg_path)
    return create_json_config()
