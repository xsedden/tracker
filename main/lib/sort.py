def sort_by_priority(input_dic):
    return input_dic["priority"]


def sort_by_deadline(input_dic):
    return input_dic["deadline"]


def sort_by_create_date(input_dic):
    return convert_date(input_dic["createdate"])


def convert_date(date):
    return date.replace(".", "")[:8]
