import subprocess
import os
from datetime import datetime
from main.lib.config import take_tasks_path, take_text_path
from main.lib.convert_data import take_last_id


class Task:

    def __init__(self, name, text, famaly, priority, deadline, creater):
        self.task_name = name
        self.task_text = text
        self.task_famaly = famaly
        self.task_priority = priority
        self.task_deadline = deadline
        self.task_creater = creater

        self.set_create_date()
        self.set_text(text)
        self.set_status(None)
        self.set_id()


    def set_create_date(self):
        """  Set create date  """
        self.create_date = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")


    def set_text(self,text):
        """  Set text by text path """
        if text:
            subprocess.call(['touch', os.path.join(take_text_path(), text)])
            subprocess.call(['vim', os.path.join(take_text_path(), text)])
            self.task_text = os.path.join(take_text_path(), text)


    def set_id_task(self):
        """  Set id task (inc last id) """
        self.task_id = 1


    def set_status(self, status):
        """  Set status for task """
        self.task_status = status


    def set_id(self):
        """  Set id task (inc last id) """
        self.task_id = take_last_id() + 1
