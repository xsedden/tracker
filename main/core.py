import logging
from main.args import parser
from main.lib.config import load_config
from main.lib.exceptions import TaskError, UserError, IDError


def main():
    try:
        arg = parser.parse_args()
        if hasattr(arg, 'set_config_path'):
            load_config(arg.set_config_path)
        else:
            if hasattr(arg, 'silence'):
                load_config(None, arg.silence)
            else:
                load_config()
        arg.func(arg)
    except TaskError as te:
        logging.info(te.msg)
        exit(1)
    except UserError as ue:
        logging.info(ue.msg)
        exit(2)
    except AttributeError:
        logging.info("Необходимо ввести аргументы. Для помощи введите аргумент -h или --help")
        exit(3)
    except IDError as ie:
        logging.info(ie.msg)
        exit(4)
